# Python Basico (Electiva 3)

Welcome to Electiva 3 Introduction to Python 3

## Objetivo del Curso

    Conceptualizar de manera abstracta/computacional diferentes problemas del mundo real.
    Crear programas complejos en Python que den solución a problemas reales.
    Familiarizarse con el uso de las estructuras de datos básicas de Python.

## Contenido:

## Unidad I: 
- Introducción a Git
- ¿Qué es Git?
- Instalacion de Git (Windows/Linux)
- Introduccion a la terminal y lineas de comandos
- Instalacion de python
- Editores de codigo
- Variables y expresiones
- Definición de funciones y módulos
- Entradas y Salidas 
- Programas interactivos con el usuario

## Unidad II
- Estructuras de Control
- Manejo de condicionales
- Ciclos
- Funciones y procesos recursivos

## Unidad III
- Estructuras Indexadas
- Listas
- Comprensión de listas
- Diccionarios
- Procesamiento de texto en Python
- Strings
- Lectura y escritura de archivos de texto

## Unidad IV
- Describir las características de una base de datos relacional y una base de documentos.
- Interactuar directamente con un motor de bases de datos desde la herramienta de administración.
- Escribir consultas simples utilizando el lenguaje SQL.
- Agregar, eliminar información y formular consultas simples de una base de datos relacional mediante el lenguaje standard SQL.
- Construir programas Python capaces de conectarse a un motor de bases de datos para extraer o registrar información.

## Unidad V: Python y la Web
- Contenido de las páginas con HTML y CSS
- El protocolo HTTP
- Arquitectura MVC/T de la aplicación Web
- El framework Django: apps, representación de datos en modelos, migraciones, views, settings, templates, forms, admin
- JavaScript y procesamiento en el lado del cliente.
- APIs REST
- Django REST Framework: endpoints, viewsets, documentación automática 

## Proyecto Final:

En grupos de max 3 personas deberan desarrollar una idea de emprendimiento cuya plataforma este desarrollada en python 3 y este puede ir desde un Algoritmos para la automatizacion de algun proceso hasta desarrollo web.

## Plan de Evaluacion:

- Miercoles, 24 NOV Unidad I (Asignacion en git 15%)
- Sabado, 27 NOV Unidad II (Asignacion en git 15%)
- Sabado, 11 DIC Unidad III (Asignacion en Git 15%)
- Sabado, 15 Enero 2022 Unidad IV (Pre defensa Proyecto 15%)
- Sabado, 22 Enero 2022 Unidad V (Entrega Proyecto Final 15%)
- Sabado, 22 Enero 2022 Defensa proyecto Final (25%)

## Bonus
- Desde 5% hasta 10% de valoracion por motivacion y empeño demostrado durante el periodo academico. 
